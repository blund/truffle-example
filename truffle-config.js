const path = require("path");
const HDWalletProvider = require('@truffle/hdwallet-provider')
const SuperProvider = require('./superprovider.js');

module.exports = {
  contracts_build_directory: path.join(__dirname, "client/src/contracts"),
  networks: {
    develop: {
      port: 8545
    },
    ropsten: {
      network_id: 3,
      provider: function() { return new HDWalletProvider(process.env.MNEMONIC, "https://ropsten.infura.io/v3/" + process.env.INFURA_API_KEY) },
      gas: 4.712e6,
      gasPrice: 15000000001
    },
    rinkeby: {
      network_id: 4,
      provider: function() { return new HDWalletProvider(process.env.MNEMONIC, "https://rinkeby.infura.io/v3/" + process.env.INFURA_API_KEY) },
      gas: 6.9e6,
      gasPrice: 15000000001
    },
    rinkeby_metamask: {
        provider: () => {
            return new SuperProvider(process.env.SUPERBLOCKS_SESSIONID, process.env.SUPERBLOCKS_ADDRESS, {proxyUrl: process.env.WEB3_ENDPOINT});
        },
        network_id: '4',
    },
    ropsten_metamask: {
        provider: () => {
            return new SuperProvider(process.env.SUPERBLOCKS_SESSIONID, process.env.SUPERBLOCKS_ADDRESS, {proxyUrl: process.env.WEB3_ENDPOINT});
        },
        network_id: '3',
    }
  }
};
